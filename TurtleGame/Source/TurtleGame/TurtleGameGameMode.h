// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TurtleGameGameMode.generated.h"

UCLASS(minimalapi)
class ATurtleGameGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATurtleGameGameMode();
};



